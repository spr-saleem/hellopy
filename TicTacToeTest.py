import unittest
from TicTacToe import Board


class MyTestCase(unittest.TestCase):
    def test_three_in_a_row_wins(self):
        board = Board("OOO")
        self.assertEqual(board.winner(), "O")

        board = Board("...XXX")
        self.assertEqual(board.winner(), "X")

        board = Board("......OOO")
        self.assertEqual(board.winner(), "O")

    def test_three_in_a_col_wins(self):
        board = Board("O..O..O")
        self.assertEqual(board.winner(), "O")

        board = Board(".X..X..X")
        self.assertEqual(board.winner(), "X")

        board = Board("..O..O..O")
        self.assertEqual(board.winner(), "O")

    def test_three_in_a_diag_wins(self):
        board = Board("O...O...O")
        self.assertEqual(board.winner(), "O")

        board = Board("..X.X.X")
        self.assertEqual(board.winner(), "X")

    def test_two_in_a_row_does_not_win(self):
        board = Board("XX")
        self.assertEqual(board.winner(), None)

        board = Board(".OO")
        self.assertEqual(board.winner(), None)

        board = Board("....XX")
        self.assertEqual(board.winner(), None)

        board = Board("......OO")
        self.assertEqual(board.winner(), None)

    def test_three_not_in_a_row_does_not_win(self):
        board = Board("XX.X")
        self.assertEqual(board.winner(), None)

        board = Board("O...O.O")
        self.assertEqual(board.winner(), None)


if __name__ == '__main__':
    unittest.main()
