class Board:
    def __init__(self, marks):
        self.marks = marks.ljust(9, ".")

    def winner(self):
        if self._col_winner() is not None:
            return self._col_winner()
        if self._row_winner() is not None:
            return self._row_winner()
        if self._diag_winner() is not None:
            return self._diag_winner()
        else:
            return None

    def _row_winner(self):
        winner = None
        for i in [0, 3, 6]:
            if self.marks[i] != "." and self.marks[i] == self.marks[i + 1] and self.marks[i] == self.marks[i + 2]:
                winner = self.marks[i]
        return winner

    def _col_winner(self):
        winner = None
        for i in [0, 1, 2]:
            if self.marks[i] != "." and self.marks[i] == self.marks[i + 3] and self.marks[i] == self.marks[i + 6]:
                return self.marks[i]
        return winner

    def _diag_winner(self):
        winner = None
        if self.marks[0] != "." and self.marks[0] == self.marks[4] and self.marks[0] == self.marks[8]:
            winner = self.marks[0]
        elif self.marks[2] != "." and self.marks[2] == self.marks[4] and self.marks[2] == self.marks[6]:
            winner = self.marks[2]
        return winner
